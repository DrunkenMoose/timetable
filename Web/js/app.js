(function() {
  var app = angular.module('rasp', []);  

  app.controller('facultyController', function(){
    this.index = 0;
    this.index1 = 0;
    this.records = [
    { title: 'ФизМат' , link: 'physmath' },
    { title: 'СоцПед' , link: 'socped' },
    { title: 'БиоФак' , link: 'bio' },
    { title: 'ФилФак' , link: 'phil' },
    { title: 'ЮрФак' , link: 'law' },
    { title: 'ФизФак' , link: 'physed' },
    { title: 'ГеоФак' , link: 'geo' },
    { title: 'ИнЯз' , link: 'forlang' },
    { title: 'ИстФак' , link: 'hist' },
    { title: 'ПсихПед' , link: 'psychped' },    
  ]; 
  });

  app.controller('groupController',function(){
  	this.a = 16;
  	 this.courses = [
  	 						{course: 1,groups: [{	name: 'МИ-11', groupId: 1},
							             {  name: 'МИ-12', groupId: 2},
							             {  name: 'ФИ-11', groupId: 3},
							             {  name: 'ЭК-11', groupId: 4},
							             {  name: 'ПМ-11', groupId: 5},
							             {  name: 'КФ-11', groupId: 6},
                						]
        			  		},
	        			  	{course: 2,groups: [{	name: 'МИ-21', groupId: 1},
								             {  name: 'МИ-22', groupId: 2},
								             {  name: 'ФИ-21', groupId: 3},
								             {  name: 'ЭК-21', groupId: 4},
								             {  name: 'ПМ-21', groupId: 5},
								             {  name: 'КФ-21', groupId: 6},
	                							]
	        			  	},
	        			  	{course: 3,groups: [{	name: 'МИ-31', groupId: 1},
								             {  name: 'МИ-32', groupId: 2},
								             {  name: 'ФИ-31', groupId: 3},
								             {  name: 'ЭК-31', groupId: 4},
								             {  name: 'ПМ-31', groupId: 5},
								             {  name: 'КФ-31', groupId: 6},
	                						]
	        			  	},
	        			  	{course: 4,groups: [{	name: 'МИ-41', groupId: 1},
								             {  name: 'МИ-42', groupId: 2},
								             {  name: 'ФИ-41', groupId: 3},
								             {  name: 'ЭК-41', groupId: 4},
								             {  name: 'ПМ-41', groupId: 5},							             
	                						]
	        			  	},
	        			  	{course: 5,groups: [{	name: 'МИ-51', groupId: 1},
								             {  name: 'МИ-52', groupId: 2},
								             {  name: 'ИИ-51', groupId: 3},
								             {  name: 'ЭК-51', groupId: 4},
								             {  name: 'ПМ-51', groupId: 5},							             
								             {  name: 'Ф-51', groupId: 6},							             
								             {  name: 'ФИ-51', groupId: 7},							             
								             {  name: 'ФМ-51', groupId: 8},							             
	                						]
	        			  	},
        			];
  });

})();
