import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class ExcelParser {
    SomeMagic someMagic;
    String fileName = "";
    public void parse(int numberOfCourse, int numberOfGroup) {
        someMagic = new SomeMagic();
        someMagic.course = numberOfCourse;
        InputStream inputStream = null;
        HSSFWorkbook workBook = null;
        try {
            inputStream = new FileInputStream(fileName);
            workBook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //разбираем первый лист входного файла на объектную модель
        Sheet sheet = workBook.getSheetAt(numberOfCourse - 1);
        Iterator<Row> it = sheet.iterator();

        StringBuilder resultString = new StringBuilder();
        //проходим по всему листу
        while (it.hasNext()) {
            Row row = it.next();
            Iterator<Cell> cells = row.iterator();
            while (cells.hasNext()) {
                Cell cell = cells.next();
                try {
                    String clearString = clearString(cell.getStringCellValue());
                    resultString.append(clearString);
                    resultString.append(" |");
                } catch (Exception e) {

                }
            }
            addToSomeMagic(resultString.toString());
            resultString = new StringBuilder();
        }
        generatedMergedCells(sheet);
        someMagic.start(numberOfGroup);
        try {
            inputStream.close();
        }catch (IOException e){

        }
    }
    void addToSomeMagic(String s){
        someMagic.add(s);
    }

    //Delete double spaces and newLine symbol
    String clearString(String s){
        StringBuilder res = new StringBuilder("");
        char[] temp = s.toCharArray();
        for (int i = 0; i < temp.length; i++) {
            if(temp[i] != '\n'){
                try{
                    if(temp[i] == ' ' && temp[i+1] == ' '){
                        continue;
                    }else{
                        res.append(temp[i]);
                    }
                }catch (IndexOutOfBoundsException e) {
                    res.append(temp[i]);
                }
            }
        }
        return res.toString().trim();
    }
    void generatedMergedCells(Sheet sheet){
        Merged merged = new Merged();
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress region = sheet.getMergedRegion(i);
            if(region.getFirstRow() >= 5) {
                merged.addIndexCells(region.getFirstRow(), region.getLastRow(), region.getFirstColumn(), region.getLastColumn());
            }
        }
        merged.sort();
        someMagic.merged = merged;
        //merged.print();
    }
}