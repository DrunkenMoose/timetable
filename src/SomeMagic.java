import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Moose on 11.09.2016.
 */
public class SomeMagic {
    ArrayList<String> data = new ArrayList<>();
    Merged merged;
    String[][] arrayData;
    int course = 0;

    void add(String s){
        if( s.contains(" пара") ||
            s.contains("ПОНЕД") ||
            s.contains("ВТОРНИК") ||
            s.contains("СРЕДА") ||
            s.contains("ЧЕТВЕРГ") ||
            s.contains("ПЯТНИЦА") ||
            s.contains("СУББОТА") //||
            //s.contains("МИ-")
          ) {
            data.add(s);
        }
    }

    void print(){
        for (int i = 0; i < data.size(); i++) {
            System.out.println(data.get(i));
        }
    }

    void start(int numOfGroup){
        //print();
        //generateHtmlBitch();
        addMergedCell();
        generateHtmlBitch(numOfGroup);
        //print();
    }
    void addMergedCell(){
        arrayData = new String[data.size()][];
        for (int j = 1; j < data.size(); j++) {
            arrayData[j] = data.get(j).split("\\|");
        }
        for (int i = 0; i < merged.cells.size() - 1 ; i++) {
            Merged.IndexCells temp = merged.cells.get(i);
            for (int j = temp.colFirst + 1; j <= temp.colLast ; j++) {
                arrayData[temp.rowFirst - 4][j] = arrayData[temp.rowFirst - 4][temp.colFirst];
            }
        }
        for (int i = 1; i < data.size(); i++) {
            for (int j = 0; j < arrayData[i].length; j++) {
                arrayData[i][j] = clearString(arrayData[i][j]);
                //System.out.print(arrayData[i][j] + " | ");
            }
            //System.out.println();
        }
    }

    String clearString(String s){
        StringBuilder res = new StringBuilder();
        char[] temp = s.toCharArray();
        for (int i = 0; i < temp.length; i++) {
            res.append(temp[i]);
            if(Character.isUpperCase(temp[i]) && Character.isLowerCase(temp[i+1])){
                res.append("<br>");
            }
        }
        return res.toString();
    }
    void generateHtmlBitch(int numOFGroup){
        for (int j = 1; j < numOFGroup; j++) {
            StringBuilder htmlCode = new StringBuilder();

            int indexDays = 0;
            String[] days = {"ВТОРНИК", "СРЕДА", "ЧЕТВЕРГ", "ПЯТНИЦА", "СУББОТА"};
            htmlCode.append("<html>\n");
            htmlCode.append("<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../css/rasp.css\">\n" +
                    "</head>");
            htmlCode.append("<body>\n<center>\n" +
                    "<div><a href = \"../../index.html\">НАЗАД</a></div>" +
                    "<table class = 'day'>\n<tr>\n <td  class = 'days' colspan = '2'>ПОНЕДЕЛЬНИК</td>\n</tr>\n");
            for (int i = 1; i < arrayData.length; i++) {
                try {
                    if (arrayData[i][0].contains(days[indexDays])) {
                        htmlCode.append("</table>\n");
                        htmlCode.append("<table class = 'day'>\n<tr>\n <td class = 'days' colspan = '2'>" + days[indexDays] + "</td>\n</tr>\n");
                        indexDays++;
                        continue;
                    }
                } catch (IndexOutOfBoundsException e) {

                }

                htmlCode.append("<tr>\n");
                htmlCode.append("<td id = 'left'>");
                htmlCode.append(addEnter(arrayData[i][0]));
                htmlCode.append("</td>\n");
                htmlCode.append("<td id = 'right'>");
                htmlCode.append(arrayData[i][j]);
                htmlCode.append("</td>\n");
                htmlCode.append("</tr>\n");
            }
            htmlCode.append("</table>");
            htmlCode.append("</center>\n</body>\n</html>");
            //System.out.println(htmlCode);
            generateIndex(htmlCode.toString(), course, j);
        }
    }
    String addEnter(String s){
        char c[] = s.toCharArray();
        StringBuilder res = new StringBuilder();
        res.append(c[0]);
        for (int i = 1; i < c.length; i++) {
            if(Character.isDigit(c[i]) && Character.isLetter(c[i-1])){
                res.append("<br>");
            }
            res.append(c[i]);
        }
        return res.toString();
    }
    void generateIndex(String html, int course, int group){
        try {
            String path = "Web/physmath/" + course + "/" + group;
            File f = new File(path);
            f.mkdirs();
            PrintWriter pw = new PrintWriter(path + "/index.html");
            pw.print(html);
            pw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
