import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Drunken_Moose on 9/15/2016.
 */
public class GroupSplitter {
    private String facultyTittle;

    private ArrayList<String[]> table;
    private String[] groups;
    public String date;
    private int currentNumFile;

    GroupSplitter(String facultyTittle) {
        this.facultyTittle = facultyTittle;
    }
    void start(){
        readFiles();
        generateHtml();
    }
    private void generateHtml(){
        for (int i = 0; i < groups.length; i++) {
            generateHtmlForGroup(i);
        }
    }
    private void generateHtmlForGroup(int numGroup){
        HtmlGenerator html = new HtmlGenerator(facultyTittle,currentNumFile, numGroup);
        html.addDateAndGroup(groups[numGroup], date);
        for(String[] data : table){
            try {
                if (data.length == 1) {
                    html.addDay(data[0]);
                } else {
                    html.addLesson(data[0], data[numGroup + 1]);
                }
            }catch (Exception e){
                continue;
            }
        }
        html.addEndHtml();
    }

    private boolean isDayOfWeek(String s){
        return s.contains("ПОНЕД") ||
                s.contains("ВТОРНИК") ||
                s.contains("СРЕДА") ||
                s.contains("ЧЕТВЕРГ") ||
                s.contains("ПЯТНИЦА") ||
                s.contains("СУББОТА");
    }

    private boolean isEmptyOrDean(String s){
        if(s.contains("ДЕКАН")){
            return true;
        }
        char[] temp = s.toCharArray();
        for (int i = 0; i < temp.length; i++) {
            if(temp[i] != ' ' && temp[i] != '|'){
                return false;
            }
        }
        return true;
    }

    private void readFiles(){
        File folder = new File("txt_files/" + facultyTittle);
        String files[] = folder.list();
        for (int i = 0; i < files.length; i++) {
            currentNumFile = i;
            readFile(files[i]);
            generateHtml();
        }
    }

    private void readFile(String fileName){
        this.table = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File("txt_files/" + facultyTittle + "/" + fileName));
            readDate(sc.nextLine());
            sc.nextLine();
            readGroups(sc.nextLine().split("\\|"));
            readTable(sc);
        }catch (IOException e){
            System.out.println("File " + facultyTittle + "not found");
        }
    }

    private void readDate(String dateString){
        date = dateString.split("\\|")[0];
    }

    private void readGroups(String[] s){
        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            if(!s[i].trim().isEmpty()){
                temp.add(s[i].trim());
            }
        }
        groups = new String[temp.size()];
        temp.toArray(groups);
    }

    private void readTable(Scanner sc){
        String s;
        while (sc.hasNextLine()){
            s = sc.nextLine().trim();
            String res[];
            if(isDayOfWeek(s)){
                res = new String[1];
                res[0] = s.split("\\|")[0];
                table.add(res);
            }else{
                if(!isEmptyOrDean(s)) {
                    res = s.split("\\|");
                    table.add(res);
                }
            }
        }
    }

    private void printTable(){
        for(String[] data : table){
            for (int i = 0; i < data.length; i++) {
                System.out.print(data[i]);
            }
            System.out.println();
        }
    }

}
