import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Drunken_Moose on 9/15/2016.
 */
public class HtmlGenerator {
    private String htmlHead = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<meta charset = \"utf-8\">" +
            "\t<title></title>\n" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../css/rasp.css\">" +
            "</head>\n" +
            "<h1 class=\"btnBack\"><a href = ../../>НАЗАД</a></h1>\n" +
            "<body>\n" +
            "\t<table>\n";
    private String facultyName;
    private int courseId;
    private int groupId;

    private StringBuffer htmlCode = new StringBuffer(htmlHead);

    HtmlGenerator(String facultyName, int courseId, int groupId) {
        this.facultyName = facultyName;
        this.courseId = courseId;
        this.groupId = groupId;
    }

    void addDay(String day){
        htmlCode.append("<td class = \"day\" colspan = \"2\">");
        htmlCode.append(day);
        htmlCode.append("</td>");
        addTr();
    }

    void addLesson(String timeLesson, String tittleLesson){
        htmlCode.append("<td class = \"time_lesson\">");
        htmlCode.append(enterBeforeTime(timeLesson));
        htmlCode.append("</td>");

        htmlCode.append("<td class = \"lesson\">");
        htmlCode.append(tittleLesson);
        htmlCode.append("</td>");
        addTr();
    }

    private String enterBeforeTime(String s){
        StringBuilder ans = new StringBuilder(s);
        ans.insert(s.indexOf("пара") + 4, "<br>");
        return ans.toString();
    }

    private void addTr(){
        htmlCode.append("<tr></tr>");
    }

    void addEndHtml(){
        htmlCode.append("</table>");
        htmlCode.append("</body>");
        htmlCode.append("</html>");
        printHtmlToFile();
    }

    private void printHtmlToFile(){
        StringBuilder path = new StringBuilder("Web/");
        path.append(facultyName);path.append("/");
        path.append(courseId + 1);path.append("/");
        path.append(groupId + 1);path.append("");
        File file = new File(path.toString());
        file.mkdirs();
        try {
            PrintWriter pw = new PrintWriter(path.toString() + "/index.html");
            pw.print(htmlCode);
            pw.close();
        }catch (IOException e){
            System.out.println("File " + path.toString() + "index.html Not Found");
        }
    }

    void addDateAndGroup(String groupName, String date){

        htmlCode.append("<div class=\"group_name\">");
        htmlCode.append(groupName);
        htmlCode.append("</div>");

        htmlCode.append("<div class=\"date\">");
        htmlCode.append(date);
        htmlCode.append("</div>");
    }

}
