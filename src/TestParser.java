import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

class TestParser {
    private String fileName;
    private String faculty;
    private ArrayList<String> noneProcessData;
    private String[][] table;

    TestParser(String faculty, String fileName) {
        this.faculty = faculty;
        this.fileName = fileName;
    }

    void parse() {
        InputStream inputStream = null;
        HSSFWorkbook workBook = null;
        try {
            inputStream = new FileInputStream(fileName);
            workBook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int numOfSheet = 0; numOfSheet < workBook.getNumberOfSheets(); numOfSheet++) {
            noneProcessData = new ArrayList<String>();
            Sheet sheet = workBook.getSheetAt(numOfSheet);
            processSheet(sheet);
            generateTable();
            processMergedCells(sheet);
            printTableToFile(sheet.getSheetName());
        }
    }
    private void printTableToFile(String sheetName){
        String path = "txt_files/" + faculty;
        File file = new File(path);
        file.mkdirs();
        PrintWriter pw = null;
        try{
            pw = new PrintWriter(path + "/" + sheetName + ".txt");
        }catch (IOException e){
            System.out.println("Error");
        }
        for (int i = 1; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                try {
                    pw.print(table[i][j] + "|");
                }catch (NullPointerException e){

                }
            }
            pw.println();
        }
        pw.close();
    }

    private void addDataInMergedCell(int i, int firstJ, int lastJ){
        for (int j = firstJ + 1; j <= lastJ; j++) {
            try {
                table[i][j] = table[i][firstJ];
            }catch (ArrayIndexOutOfBoundsException e){

            }
        }
    }

    private void processMergedCells(Sheet sheet){
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress region = sheet.getMergedRegion(i);
            addDataInMergedCell(region.getFirstRow(), region.getFirstColumn(), region.getLastColumn());
        }
    }

    private void generateTable(){
        table = new String[noneProcessData.size()][];
        int i = 0;
        for(String s : noneProcessData){
            table[i] = s.split("\\|");
            i++;
        }
    }

    private void processSheet(Sheet sheet){
        for(Row row : sheet){
            StringBuilder rowString = new StringBuilder("");
            for(Cell cell : row){
                try {
                    String clearString = deleteSpacesAndNewLine(cell.getStringCellValue());
                    rowString.append(clearString);
                    rowString.append(" | ");
                }catch(IllegalStateException e){

                }
            }
            noneProcessData.add(rowString.toString());
            //System.out.println(rowString);
        }
    }

    private String deleteSpacesAndNewLine(String s){
        char[] sChas = s.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < sChas.length; i++) {
            try {
                if (sChas[i] != ' ' || sChas[i + 1] != ' ') {
                    if (sChas[i] == '\n') {
                        result.append(" ");
                    } else {
                        result.append(sChas[i]);
                    }
                }
            }catch (IndexOutOfBoundsException e){

            }
        }
        return result.toString();
    }
}