import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Moose on 11.09.2016.
 */
public class Merged {
    //Объединённые ячейки
    class IndexCells{
        int colFirst, colLast;
        int rowFirst, rowLast;
        public IndexCells(int rowFirst, int rowLast, int colFirst, int colLast) {
            this.rowFirst = rowFirst;
            this.rowLast = rowLast;
            this.colFirst = colFirst;
            this.colLast = colLast;
        }
    }
    ArrayList<IndexCells> cells = new ArrayList<>();

    public Merged() {
    }
    void addIndexCells(int rowFirst, int rowLast, int colFirst,int colLast){
        cells.add(new IndexCells(rowFirst, rowLast, colFirst, colLast));
    }
    void sort(){
        cells.sort(new Comparator<IndexCells>() {
            @Override
            public int compare(IndexCells o1, IndexCells o2) {
                if(o1.rowFirst < o2.rowFirst) return -1;
                if(o1.rowFirst > o2.rowFirst) return 1;
                if(o1.colFirst < o2.colFirst) return -1;
                if(o1.colFirst > o2.colFirst) return 1;
                return 0;
            }
        });
    }

    void print(){
        for (int i = 0; i < cells.size(); i++) {
            IndexCells temp = cells.get(i);
            System.out.println(temp.rowFirst + " " + temp.colFirst + " " + temp.colLast);
        }
    }
}
